from django.db import models

# Create your models here.

class Equipo(models.Model):
    id = models.AutoField(primary_key=True)
    nombreequipo = models.CharField(max_length=24)
    numerojugadores = models.IntegerField(null=True)
    entrenador = models.CharField(max_length=24)
    campeonatos = models.CharField(max_length=24)
    temporada = models.BooleanField(default=True)
    slug = models.SlugField()

    def __str__(self):
        return self.nombreequipo

class Jugador(models.Model):
    id = models.AutoField(primary_key=True)
    nombrejugador = models.CharField(max_length=24)
    apellidojugador = models.CharField(max_length=24)
    apodo = models.CharField(max_length=24)
    numero = models.IntegerField(null=True)
    posicion = models.CharField(max_length=24)
    nombreequipo = models.CharField(max_length=24)
    equipoanterior = models.CharField(max_length=24)
    activo = models.BooleanField(default=True)
    slug = models.SlugField()

    def __str__(self):
        return self.nombrejugador

class Estadio(models.Model):
    id = models.AutoField(primary_key=True)
    nombreestadio = models.CharField(max_length=24)
    nombreequipo = models.CharField(max_length=24)
    capacidad = models.IntegerField(null=True)
    mascota = models.CharField(max_length=24)
    activo = models.BooleanField(default=True)
    slug = models.SlugField()

    def __str__(self):
        return self.nombreestadio
