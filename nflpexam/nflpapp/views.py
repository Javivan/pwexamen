from django.shortcuts import render
from .models import Jugador
from .models import Equipo
from .models import Estadio

# Create your views here.

def listajugadores(request):
	return render(request,'nflptemp/1.html', {"jugadores": Jugador.objects.all()})

def detallesjugadores(request,id):
	return render(request,'nflptemp/2.html', {"jugador": Jugador.objects.get(id=id)})

def listaequipos(request):
	return render(request,'nflptemp/3.html',{"equipos": Equipo.objects.all()})

def detallesequipos(request,id):
	return render(request,'nflptemp/4.html',{"equipo": Equipo.objects.get(id=id)})

def listaestadios(request):
	return render(request,'nflptemp/5.html',{"estadios": Estadio.objects.all()})

def detallesestadios(request,id):
	return render(request,'nflptemp/6.html',{"estadio": Estadio.objects.get(id=id)})
