from django.urls import path
from nflpapp import views

urlpatterns = [
    path('1/',views.listajugadores, name='listajugadores'),
    path('2/<int:id>',views.detallesjugadores, name='detallesjugadores'),
    path('3/',views.listaequipos, name='listaequipos'),
    path('4/<int:id>',views.detallesequipos, name='detallesequipos'),
    path('5/',views.listaestadios, name='listaestadios'),
    path('6/<int:id>',views.detallesestadios, name='detallesestadios'),
]
