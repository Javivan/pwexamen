from django.apps import AppConfig


class NflpappConfig(AppConfig):
    name = 'nflpapp'
